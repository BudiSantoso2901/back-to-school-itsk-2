<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('bahan_presentasis', function (Blueprint $table) {
            $table->id();
            $table->string('nama_materi');
            $table->enum('status',['Aktif','NonAktif'])->default('Aktif');
            $table->string('surat_tugas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('bahan_presentasis');
    }
};
