<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('bahan_presentasi_dan_file_materis', function (Blueprint $table) {
            $table->id();
            $table->foreignId('bahan_presentasi_id')->constrained('bahan_presentasis');
            $table->foreignId('file_materi_id')->constrained('file_materis');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('bahan_presentasi_dan_file_materis');
    }
};
