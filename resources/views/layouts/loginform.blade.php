<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap 5 CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Back To School CSS -->
    <link rel="stylesheet" href="../../css/app.css">

    <title>

        @yield('title')

    </title>
</head>
<body>
    <div class="container-fluid bg-light">
        <div class="row">
            <div class="col-sm-6 full-height d-flex flex-column justify-content-center align-items-center text-black">

                @yield('container')

            </div>
            <div class="col-sm-6 full-height main-img"></div>
        </div>
    </div>
    
    <!-- Bootstrap 5 JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>