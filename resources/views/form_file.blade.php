<!-- resources/views/bahan-presentasi/create.blade.php -->
<form action="{{ route('file.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div>
        <label for="nama_materi">Nama Materi:</label>
        <input type="text" id="nama_materi" name="nama_materi" required>
    </div>
    <div>
        <label for="status">Status:</label>
        <select id="status" name="status" required>
            <option value="Aktif">Aktif</option>
            <option value="NonAktif">Non-Aktif</option>
        </select>
    </div>
    <div>
        <label for="surat_tugas">Surat Tugas:</label>
        <input type="file" id="surat_tugas" name="surat_tugas" required>
    </div>
    <div>
        <label for="files">File Materi:</label>
        <input type="file" id="files" name="files[]" multiple required accept=".doc,.pdf,.ppt">
    </div>
    <button type="submit">Submit</button>
</form>
