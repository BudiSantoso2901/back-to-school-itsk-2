<section id="header">
    <header >
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <!-- Kolom pertama -->
                <div class="col-lg-6">
                    <div class="text-lg-left">
                        <h1 class="pb-3">Back To School</h1>
                        <p class="pb-3">Program "Back to school" merupakan sebuah program yang diinisiasi oleh kampus dengan tujuan mempromosikan institusi kepada sekolah-sekolah asal para mahasiswa.</p>
                        <a href="#" class="btn btn-dark px-4 mb-5">Daftar</a>
                    </div>
                </div>
                <!-- Kolom kedua -->
                <div class="col-lg-6">
                    <img src="https://images.unsplash.com/photo-1530099486328-e021101a494a?q=80&w=2147&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D" alt="Gambar Header" class="img-fluid">
                </div>
            </div>
        </div>
    </header>
    
</section>