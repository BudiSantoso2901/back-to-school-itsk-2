<style>
    .list-group-item a {
        text-decoration: none; 
        color: #000;
    }
</style>

<section id="faq">
    <div class="container pb-5">
        <h1 class="pb-3 pt-3">Daftar Pertanyaan</h1>
        <ul class="list-group pb-5 mx-5">
            <li class="list-group-item"><a href="#pertanyaanModal1" data-toggle="modal" data-target="#pertanyaanModal1">1. Bagaimana proses pendaftaran dan partisipasi dalam program ini?</a></li>
            <li class="list-group-item"><a href="#pertanyaanModal2" data-toggle="modal" data-target="#pertanyaanModal2">2. Bagaimana alur kegiatan Back To School?</a></li>
            <li class="list-group-item"><a href="#pertanyaanModal3" data-toggle="modal" data-target="#pertanyaanModal3">3. Apa saja kegiatan yang biasanya dilakukan dalam program Back To School?</a></li>
            <li class="list-group-item"><a href="#pertanyaanModal4" data-toggle="modal" data-target="#pertanyaanModal4">4. Adakah informasi lebih lanjut tentang jadwal dan kegiatan yang akan diadakan selama pelaksanaan program ini?</a></li>
            <li class="list-group-item"><a href="#pertanyaanModal5" data-toggle="modal" data-target="#pertanyaanModal5">5. Bagaimana proses pendaftaran dan partisipasi dalam program ini?</a></li>
        </ul>
    </div>
</section>

<!-- Modal Pertanyaan 1 -->
<div class="modal fade" id="pertanyaanModal1" tabindex="-1" role="dialog" aria-labelledby="pertanyaanModal1Label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="pertanyaanModal1Label">Bagaimana proses pendaftaran dan partisipasi dalam program ini?</h5>
          <button type="button" class="close badge bg-dark" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            Proses pendaftaran dalam program ini dimulai dengan mengeklik tombol daftar pada halaman beranda, yang akan mengarahkan anda ke halaman formulir pendaftaran. Di sana, anda akan diminta untuk mengisi formulir sesuai dengan data anda. Setelah anda mengirimkan formulir, dosen pendamping akan meninjau pendaftaran anda untuk melakukan persetujuan. Setelah pendaftaran anda disetujui, anda dapat langsung mulai berpartisipasi dalam kegiatan di sekolah pilihan anda.
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        </div>
      </div>
    </div>
</div>

<!-- Modal Pertanyaan 2 -->
<div class="modal fade" id="pertanyaanModal2" tabindex="-1" role="dialog" aria-labelledby="pertanyaanModal2Label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="pertanyaanModal2Label">Bagaimana alur kegiatan Back To School?</h5>
          <button type="button" class="close badge bg-dark" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            pertama peserta melakukan pendaftaran pada website back to school,  dilanjutkan dengan peninjauan dan penyetujuan pendaftaran oleh dosen pendamping. Setelah itu, kegiatan dilaksanakan di sekolah yang dipilih. Setelah selesai, peserta diharuskan mengunggah laporan kegiatan dan akan mendapatkan sertifikat partisipasi.
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        </div>
      </div>
    </div>
</div>

<!-- Modal Pertanyaan 3 -->
<div class="modal fade" id="pertanyaanModal3" tabindex="-1" role="dialog" aria-labelledby="pertanyaanModal3Label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="pertanyaanModal3Label">Apa saja kegiatan yang biasanya dilakukan dalam program Back To School?</h5>
          <button type="button" class="close badge bg-dark" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            Dalam program ini, peserta akan membawa nuansa kampus langsung ke dalam ruang kelas di berbagai sekolah di seluruh Indonesia. Mereka akan berbagi kisah inspiratif dan pengalaman pribadi yang memukau kepada para siswa SMA, memberikan wawasan unik tentang kehidupan kampus dan peluang pendidikan yang menarik.
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        </div>
      </div>
    </div>
</div>

<!-- Modal Pertanyaan 4 -->
<div class="modal fade" id="pertanyaanModal4" tabindex="-1" role="dialog" aria-labelledby="pertanyaanModal4Label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="pertanyaanModal4Label">Adakah informasi lebih lanjut tentang jadwal dan kegiatan yang akan diadakan selama pelaksanaan program ini?</h5>
          <button type="button" class="close badge bg-dark" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            Informasi lebih lanjut akan diberitahukan melalui fitur pemberitahuan atau dosen pendamping program.
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        </div>
      </div>
    </div>
</div>

<!-- Modal Pertanyaan 5 -->
<div class="modal fade" id="pertanyaanModal5" tabindex="-1" role="dialog" aria-labelledby="pertanyaanModal5Label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="pertanyaanModal5Label">Bagaimana proses pendaftaran dan partisipasi dalam program ini?</h5>
          <button type="button" class="close badge bg-dark" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            Anda dapat bertanya pada kontak yang tersedia atau juga bisa melalui fitur pertanyaan yang sudah di sediakan.
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        </div>
      </div>
    </div>
</div>
  
<section id="pesan">
    <div class="container pb-5">
        <div class="text-center">
            <h2 class="pb-3">Ajukan Pertanyaan Anda</h2>
            <p class="pb-3">Jika informasi yang disampaikan kurang jelas anda bisa mengirim pertanyaan pada kolom berikut</p>
        </div>
        <form>
            <div class="input-group mb-3">
                <input type="text" class="form-control" placeholder="Masukkan pertanyaan anda" aria-label="Recipient's username" aria-describedby="button-addon2">
                <button class="btn btn-dark px-5" type="button" id="button-addon2">Kirim</button>
            </div>
        </form>
    </div>
</section>
    

