<style>
    #riwayatpendaftaran {
        height: 100vh;
        overflow-y: auto;
        overflow-x: hidden;
        
    }

    .col {
        border-right: 1px solid #ccc; /* Menambahkan garis antar kolom */
    }

    /* Menghilangkan garis di kolom terakhir */
    .col:last-child {
        border-right: none;
    }
</style>
<section id="riwayatpendaftaran">
    <div class="container">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h5 class="mb-0"></h5>
                <h5 class="mb-0">Riwayat Pendaftaran</h5>
                <span><a href="#" class="btn btn-dark px-5">Daftar</a></span>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col">Nomer</div>
                    <div class="col">Nama Sekolah</div>
                    <div class="col">Provinsi</div>
                    <div class="col">Tanggal Pengajuan</div>
                    <div class="col">Status</div>
                    <div class="col">Detail</div>
                </div>
                {{-- @foreach($kegiatans as $kegiatan)
                <div class="row">
                    <div class="col-md-1">{{ $kegiatan->id }}</div>
                    <div class="col-md-3">{{ $kegiatan->sekolah }}</div>
                    <div class="col-md-3">{{ $kegiatan->provinsi->nama }}</div>
                    <div class="col-md-3">{{ $kegiatan->tanggal_kegiatan }}</div>
                    <div class="col-md-2">
                        @if($kegiatan->status_promosi == 'Berlangsung')
                        <span class="badge badge-warning">{{ $kegiatan->status_promosi }}</span>
                        @elseif($kegiatan->status_promosi == 'Diproses')
                        <span class="badge badge-info">{{ $kegiatan->status_promosi }}</span>
                        @elseif($kegiatan->status_promosi == 'Ditolak')
                        <span class="badge badge-danger">{{ $kegiatan->status_promosi }}</span>
                        @elseif($kegiatan->status_promosi == 'Selesai')
                        <span class="badge badge-success">{{ $kegiatan->status_promosi }}</span>
                        @endif
                    </div>
                    <div class="col">
                        <a href="{{ route('detail.kegiatan', $kegiatan->id) }}" class="btn btn-info">Lihat</a>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
</section>

