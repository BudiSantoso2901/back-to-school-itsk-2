<style>
    #detailkegiatan {
        height: 100vh;
        overflow-y: auto;
        overflow-x: hidden;
    }
</style>
<section id="detailkegiatan">
    <div class="card container">
        <div class="card-header bg-white mt-3">
            <h5 class="card-title">Detail Kegiatan</h5>
        </div>
        <div class="card-body">
            <ul class="list-group list-group-flush">
                <li class="list-group p-3">Nama Ketua: </li>
                <li class="list-group p-3">Nama Anggota: </li>
                <li class="list-group p-3">Nama Dosen Pembimbing: </li>
                <li class="list-group p-3">Nama Sekolah: </li>
                <li class="list-group p-3">Tanggal Pelaksanaan: </li>
                <li class="list-group p-3">Status Kegiatan: </li>
            </ul>
        </div>
        <div class="card-header bg-white mt-3">
            <h5 class="card-title">File Pendukung</h5>
        </div>
        <div class="card-body">
            <a href="#" class="badge bg-dark text-decoration-none"><i data-feather="download"></i> Download PPT</a> 
        </div>
        <div class="card-header bg-white mt-3">
            <h5 class="card-title">Laporan Kegiatan</h5>
        </div>
        <div class="card-body">
            <a href="#" class="badge bg-success text-decoration-none"><i data-feather="plus"></i> Tambah Laporan</a> 
        </div>
    </div>
</section>