<style>
    #formpendaftaran {
        height: 100vh;
        overflow-y: auto;
        overflow-x: hidden;
    }
</style>
<section id="formpendaftaran">
    <div class="container mt-5">
        <div class="row justify-content-center">
          <div class="col-md-6">
            <div class="card">
              <div class="card-body p-5">
                <h5 class="card-title"><h2 class="text-center">Form Pendaftaran</h2></h5>
                <form action="{{ route('kegiatan.store') }}" method="POST">
                    @csrf
                  <div class="form-group pt-3">
                    <label for="nama"><h5>Nama: </h5> </label>
                    {{-- <input type="text" class="form-control" name="nama" id="id_user"  value="{{ Auth::user()->nama }}" placeholder="Masukkan Nama" required readonly> --}}
                  </div>
                  <div class="form-group pt-3">
                    <label for="email"><h5>Email: </h5> </label>
                    {{-- <input type="email" class="form-control" name="email" id="id_user" value="{{ Auth::user()->email }}" placeholder="Masukkan Email" required readonly> --}}
                  </div>
                  <div class="form-group pt-3">
                    <label for="tanggal"><h5>Tanggal Pelaksanaan:</h5></label>
                    <input type="date" class="form-control" name="tanggal_kegiatan" id="tanggal" required>
                </div>
                <div class="form-group pt-3">
                    <label for="provinsi"><h5>Provinsi Sekolah:</h5></label>
                    <select class="form-control" name="id_provinsi" id="id_provinsi" required>
                        <option value="">Pilih</option>
                        {{-- @foreach ($prv as $provinsi)
                            <option value="{{ $provinsi->id }}">{{ $provinsi->provinsi }}</option>
                        @endforeach --}}
                    </select>
                </div>
                <div class="form-group pt-3">
                    <label for="sekolah"><h5>Asal Sekolah:</h5></label>
                    <input type="text" class="form-control" name="sekolah" id="sekolah" value="" placeholder="Masukkan Sekolah">
                </div>
                <div class="form-group pt-3">
                    <label for="anggota"><h5>Anggota:</h5></label>
                    <select class="form-control" name="id_user[]" id="id_user" required multiple>
                        <option value="">Masukkan Nama Anggota Anda</option>
                        {{-- @foreach ($usr as $user)
                            <option value="{{ $user->id }}">{{ $user->nama }}</option>
                        @endforeach --}}
                    </select>
                </div>
                <button type="submit" class="btn btn-dark d-block mx-auto px-5 mt-5">Kirim</button>
            </form>
              </div>
            </div>
          </div>
        </div>
      </div>
</section>
<script>
    document.addEventListener("DOMContentLoaded", function() {
        document.getElementById('id_user').addEventListener('change', function() {
            var selectedOption = this.options[this.selectedIndex];
            document.getElementById('nama').value = selectedOption.getAttribute('data-nama');
            document.getElementById('email').value = selectedOption.getAttribute('data-email');
        });
    });
</script>

