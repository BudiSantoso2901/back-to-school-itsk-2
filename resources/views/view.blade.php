@extends('layouts.mainform')

@section('title', 'BTS-ITSK | Form Pengajuan')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Data Kegiatan</div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Provinsi</th>
                                <th scope="col">Tanggal Kegiatan</th>
                                <th scope="col">Sekolah</th>
                                <th scope="col">Status Promosi</th>
                                <th scope="col">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $key => $kegiatan)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $kegiatan->provinsi->provinsi }}</td>
                                    <td>{{ $kegiatan->tanggal_kegiatan }}</td>
                                    <td>{{ $kegiatan->sekolah }}</td>
                                    <td>{{ $kegiatan->status_promosi }}</td>
                                    <td>
                                        <a href="" class="btn btn-primary">Detail</a>

                                        {{-- {{ route('kegiatan.edit', $kegiatan->id) }} --}}
                                        {{-- <form action="{{ route('kegiatan.delete', $kegiatan->id) }}" method="POST" class="d-inline"> --}}
                                            @csrf
                                            {{-- @method('DELETE') --}}
                                            {{-- <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this item?')">Delete</button> --}}
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- @endsection --}}
