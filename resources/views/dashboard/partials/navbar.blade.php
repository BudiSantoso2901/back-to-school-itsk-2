<section id="admin-navbar">
    <section id="navbar">
        <nav class="navbar navbar-expand-lg navbar-light bg-warning"> <!-- Menggunakan class "bg-warning" untuk latar belakang oranye -->
            <div class="container">
                <div class="ms-auto"> <!-- Menggunakan class "ms-auto" untuk menggeser elemen ke kanan -->
                    <div class="dropdown">
                        <img src="img/logoputih.png" alt="Profile Image" id="profileImage" class="rounded-circle" height="60" data-toggle="dropdown">
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#">Profile</a>
                            <a class="dropdown-item" href="#">Logout</a>
                        </div>
                    </div>
                </div>
            </div>
        </nav>    
    </section>
</section>
