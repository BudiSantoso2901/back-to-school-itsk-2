<?php

use App\Http\Controllers\KontenController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserManagementController;
use App\Http\Controllers\ForgotPasswordController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


Route::get('/', function () {
    if (Auth::check()) {
        return view('index');
    } else {
        return view('welcome');
    }
});

// LOG IN
Route::get('/signin', [AuthController::class, 'loginPage'])->name('login');
Route::post('/signin', [AuthController::class, 'prosesLogin'])->name('login.submit');

// REGISTER
Route::get('/signup', [AuthController::class, 'registerPage'])->name('register');
Route::post('/signup', [AuthController::class, 'processRegister'])->name('register.submit');

// LOG OUT
Route::post('/logout', [AuthController::class, 'logout'])->name('logout');

// FORGOT-PASSWORD
Route::get('/forgot-password', [ForgotPasswordController::class, 'forgotPasswordPage'])->name('forgot-password');
Route::post('/forgot-password-act', [ForgotPasswordController::class, 'resetAction'])->name('forgot-password-act');

Route::get('/validasi-forgot-password/{token}', [ForgotPasswordController::class, 'validasiForgotPassword'])
    ->name('validasi-forgot-password');

Route::post('/validasi-forgot-password-act', [ForgotPasswordController::class, 'validasiAction'])
    ->name('validasi-forgot-password-act');

Route::get('/status', function () {
    return view('statuspengajuan');
});

Route::get('/formpengajuan', function () {
    return view('formpengajuan');
});

Route::get('/detailkegiatan', function () {
    return view('detailkegiatan');
});


Route::get('/profile', function () {
    return view('userprofile');
});

Route::prefix('/User')->middleware('auth', 'check-access:mahasiswa')->group(function () {
    Route::get('/view', [KontenController::class, 'kegiatan_view'])->name('view');
    Route::get('/form', [KontenController::class, 'kegiatanUser_Add_View'])->name('formpengajuan');
    Route::post('/store/kegiatan', [KontenController::class, 'KegiatanUser_Store'])->name('kegiatan.store');
});

Route::prefix('/admin')->middleware(['auth', 'check-access:admin'])->group(function () {
    Route::get('/form', [KontenController::class, 'view_form_bahan_presentasi'])->name('form_file');
    Route::post('/store/file', [KontenController::class, 'store_bahan_presentasi'])->name('file.store');
});

Route::get('/usersmanagement', [UserManagementController::class, 'index'])->name('usersmanagement.index');
Route::get('/usersmanagement/create', [UserManagementController::class, 'create'])->name('usersmanagement.create');
Route::post('/usersmanagement', [UserManagementController::class, 'store'])->name('usersmanagement.store');
Route::get('/usersmanagement/{id}/edit', [UserManagementController::class, 'edit'])->name('usersmanagement.edit');
Route::put('/usersmanagement/{id}', [UserManagementController::class, 'update'])->name('usersmanagement.update');
Route::delete('/usersmanagement/{id}', [UserManagementController::class, 'destroy'])->name('usersmanagement.destroy');

// ======================== ADMIN DASHBOARD  ========================
Route::view('/dashboard', 'dashboard.dashboard');
