<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User;
use Illuminate\Contracts\Auth\Authenticatable;

class UserManagement extends Model
{
    use HasFactory;

    protected $table = 'users';

    protected $fillable = ['nama', 'nim', 'email', 'password', 'role'];

    
}
