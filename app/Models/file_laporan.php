<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class file_laporan extends Model
{
    use HasFactory;

    public function laporan()
    {
        return $this->belongsTo(laporan::class, 'id_file');
    }
}
