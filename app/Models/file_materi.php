<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class file_materi extends Model
{
    use HasFactory;

    protected $fillable = ['dokumen'];

    public function bahanPresentasis()
    {
        return $this->belongsToMany(bahan_presentasi::class, 'bahan_presentasi_dan_file_materis', 'file_materi_id', 'bahan_presentasi_id')->withTimestamps();
    }
}
