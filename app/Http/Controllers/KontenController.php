<?php

namespace App\Http\Controllers;

use App\Models\bahan_presentasi;
use App\Models\sekolah;
use Illuminate\Http\Request;
use App\Models\kegiatan;
use App\Models\User;
use App\Models\provinsi;
use App\Models\laporan;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
class KontenController extends Controller
{
    //view kegiatan
    // public function kegiatan_view()
    //     {
    //     $data['kgt'] = kegiatan::all();
    //     return view("view", $data);
    //     }
    public function kegiatan_view()
        {
            $data = Kegiatan::with(['users', 'provinsi'])->get();
            return view('view', compact('data'));
        }
    //add kegiatan dari user
    function kegiatanUser_Add_View() {
       $usr = User::all();
       $prv = provinsi::all();
    return view ('formpengajuan',compact('usr','prv'));
    }

    public function KegiatanUser_Store(Request $request){
        $request->validate([
            'id_provinsi' => 'required|exists:provinsis,id',
            'id_user' => 'required|exists:users,id',
            'tanggal_kegiatan' => 'required|date',
            'sekolah' => 'required',
        ]);

        $kegiatan = kegiatan::create([
            'id_provinsi' => $request->id_provinsi,
            'tanggal_kegiatan' => $request->tanggal_kegiatan,
            'sekolah' => strtoupper($request->sekolah),
            'status_promosi' => 'Pending',
        ]);

        $kegiatan->users()->attach($request->id_user);
    return redirect()->route('view')->with('success', 'Tambah Kegiatan berhasil');
}
    public function JadwalVIew()
        {
            $alldatajadwal = kegiatan::with(['', ''])->get();
            return view('user.jadwal', compact('alldatajadwal'));
        }


//Materi Kegiatan User
    public function view_form_bahan_presentasi() {
        return view('form_file');
    }
    public function store_bahan_presentasi(Request $request) {
        $request->validate([
            'nama_materi' => 'required|string',
            'status' => 'required|in:Aktif,NonAktif',
            'surat_tugas' => 'required|file|mimes:pdf,doc,docx',
            'files.*' => 'required|file|mimes:pdf,doc,docx,ppt'
        ]);
        // Upload surat tugas
        $srt = time() . '_' . $request->file('surat_tugas')->getClientOriginalName();
        $request->file('surat_tugas')->storeAs('public/files', $srt);

        $bahanPresentasi = bahan_presentasi::create([
            'nama_materi' => $request->nama_materi,
            'status' => $request->status,
            'surat_tugas' => $srt,
        ]);

        // Upload file materi
        foreach ($request->file('files') as $file) {
            $fileName = time() . '_' . $file->getClientOriginalName();
            $file->storeAs('public/files', $fileName);
            $bahanPresentasi->fileMateris()->create(['dokumen' => $fileName]);
        }

        return redirect()->route('view')->with('success', 'Bahan presentasi berhasil ditambahkan.');
    }

    //Edit Status Untuk Dosen
    public function editPromosi($id)
    {
        $kegiatan = Kegiatan::findOrFail($id);
        return view('edit_promosi', compact('kegiatan'));
    }

    public function updatePromosi(Request $request, $id)
    {
        $request->validate([
            'status_promosi' => 'required|in:Pending,Approved,Rejected',
        ]);

        $kegiatan = Kegiatan::findOrFail($id);
        $kegiatan->status_promosi = $request->status_promosi;
        $kegiatan->save();

        return redirect()->route('view')->with('success', 'Status promosi kegiatan berhasil diperbarui');
    }
}
