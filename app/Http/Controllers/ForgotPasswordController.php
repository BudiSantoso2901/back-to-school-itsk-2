<?php

namespace App\Http\Controllers;

use App\Models\PasswordResetToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ForgotPasswordMail;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class ForgotPasswordController extends Controller
{
    public function forgotPasswordPage()
    {
        return view("forgotpassword");
    }

    public function resetAction(Request $request)
    {
        $customMessage = [
            'email.required'    => 'Email tidak boleh kosong',
            'email.exists'       => 'Email tidak terdaftar di Database',
        ];

        $request->validate([
            'email' => 'required|email|exists:users,email',
        ], $customMessage);

        $token = Str::random(60);

        PasswordResetToken::updateOrCreate(
            [
                'email' => $request->email,
            ],
            [
                'email'         => $request->email,
                'token'         => $token,
                'created_at'    => now(),
            ]
        );

        Mail::to($request->email)->send(new ForgotPasswordMail($token));

        return redirect()->route('forgot-password')
            ->with('success', 'Link Reset Password telah terkirim ke email anda');
    }

    public function validasiForgotPassword(Request $request, $token)
    {
        $getToken = PasswordResetToken::where('token', $token)->first();

        if (!$getToken) {
            return redirect()->route('login')->with('failed', 'Token tidak Valid');
        }

        return view('changepassword', compact('token'));
    }

    public function validasiAction(Request $request)
    {
        $customMessage = [
            'password.required'    => 'Password tidak boleh kosong',
            'password.min'       => 'Password minimal 6 karakter',
        ];

        $request->validate([
            'password'  =>  'required|min:6'
        ], $customMessage);

        // dd($request->all());

        $token = PasswordResetToken::where('token', $request->token)->first();

        if (!$token) {
            return redirect()->route('login')->with('failed', 'Token tidak Valid');
        }

        $user = User::where('email', $token->email)->first();

        if (!$user) {
            return redirect()->route('login')->with('failed', 'Email tidak terdaftar di Database kami');
        }

        $user->update([
            'password'=> Hash::make($request->password)
        ]);

        $token->delete();

        return redirect()->route('login')->with('success','Password Berhasil di reset!');
    }
}
