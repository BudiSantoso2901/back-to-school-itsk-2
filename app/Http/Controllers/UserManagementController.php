<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\UserManagement;

class UserManagementController extends Controller
{
    public function index()
    {
        // Ambil data pengguna tanpa kolom password
        $users = UserManagement::select('id', 'nama', 'nim', 'email', 'role')->get();
        
        // Kirim data ke view usersmanagement
        return view('usersmanagement', compact('users'));
    }    

    public function create()
    {
        return view('formtambahuser');
    }

    public function store(Request $request)
    {
        // Validasi data request di sini jika diperlukan
    
        // Enkripsi password sebelum menyimpan ke database
        $request->merge(['password' => Hash::make($request->password)]);
    
        // Simpan user baru
        UserManagement::create($request->all());
    
        // Arahkan pengguna kembali ke halaman usersmanagement
        return redirect()->route('usersmanagement.index')->with('success', 'User berhasil ditambahkan!');
    }    
    
    public function edit($id)
    {
        $user = UserManagement::findOrFail($id);
        return view('formedituser', compact('user'));
    }
    
    public function update(Request $request, $id)
    {
        $user = UserManagement::findOrFail($id);
        $user->update($request->only(['nama', 'nim', 'email', 'role']));
        return redirect()->route('usersmanagement.index')->with('success', 'Informasi pengguna berhasil diperbarui!');
    }
    
    public function destroy(UserManagement $user)
    {
        // Hapus user
        $user->delete();
    
        // Arahkan pengguna kembali ke halaman usersmanagement
        return redirect()->route('usersmanagement.index')->with('success', 'User berhasil dihapus!');
    }
    
}

